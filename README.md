pass-gocrypt
===

An extension for [pass](https://www.passwordstore.org/) that hides part of the password files inside a subdirectory encrypted by
[gocryptfs](https://github.com/rfjakob/gocryptfs).

`pass-gocrypt` serves a somewhat similar purpose as [pass-coffin](https://github.com/ayushnix/pass-coffin) and
[pass-tomb](https://github.com/roddhjav/pass-tomb) -- because `pass` does not encrypt the directory structure (metadata) by
default, anyone who has access to the password store is able to deduce what websites / services does the owner have accounts on.
Both `pass-coffin` and `pass-tomb` provide workarounds to this limitation by placing the entire password store inside an encrypted
format when unused, shielding metadata from offline attacks.

This approach, in general, works perfectly fine, but comes with some shortcomings that, in the author's view, defeat the point of
using `pass`. For example, when a password store is locked inside a `coffin` or a `tomb`, you lose the ability to synchronize
the store using `git`, at least not without seriously compromising the security model of `coffin` and `tomb`. Furthermore, the
all-or-nothing approach renders the entire password store unusable on mobile clients of `pass` (again, at least without compromising
`coffin` and `tomb` themselves).

In `pass-gocrypt`, only a subtree of the password store will be encrypted. This encryption is done transparently with `gocryptfs`,
with a password generated and managed by `pass` itself. You can optionally supply another passphrase when initializing this
extension, which will be used along with the one managed by `pass` to derive the symmetric encryption key used by `gocryptfs`.

When the encrypted subtree is unlocked, it simply appears as a subdirectory
of the original password store (`gocrypt/`), and all read operations from `pass`, including from web browser plugins, can be done
without any special care (other than remembering to unlock the subtree first). The encrypted subdirectory is stored in the original
password store under `.gocrypt/`, and can be managed by `git` just like how it was without encryption.

The biggest caveat of this is that write operations (such as `edit` and `generate`) **have** to be prefixed by the `gocrypt` subcommand
to ensure compatibility when the outer password store is a git repository. Without the prefix, git commits that are normally created
automatically by `pass` will not be generated during a write. See the Usage section of this document for examples.

Installation
===

This extension is consisted of only one script, `gocrypt.bash`, and can be simply copied into the `.extensions` folder inside your
password store to be installed. You will need `PASSWORD_STORE_ENABLE_EXTENSIONS` to be set to `true` for `pass` to load the extension.

Alternatively, the extension can be installed to the system extension directory in `/usr/lib/password-store/extensions`.

Dependencies:

- pass
- bash
- gocryptfs

Usage
===

Please run `pass gocrypt help` after installation for detailed help. Below is a simple example of using `pass-gocrypt` to encrypt
a subset of your passwords.

To initialize:

```sh
pass gocrypt init
# With extra passphrase:
# pass gocrypt init -p
```

To generate a password inside the encrypted subdirectory:

```sh
pass gocrypt generate "My/Password"
```

To view a password from inside the encrypted subdirectory:

```sh
pass gocrypt show "My/Password"
# or simply: pass show "gocrypt/My/Password"
# or from any other pass-compatible GUI, when the subdirectory is opened
```

To move a password from outside of the encrypted subdirectory to inside:

```sh
pass gocrypt crypt "My/Insecure Password"
```

To close (unmount) the encrypted subdirectory:

```sh
pass gocrypt close
```

To re-open (mount) the encrypted subdirectory:

```sh
pass gocrypt open
```

To make the encrypted subtree a git repository of its own:

```sh
pass gocrypt git init
```

__Note__: This will allow changes inside the subdirectory to be recorded separately from the git repo of the outside password
store. This has the benefit of retaining the unencrypted changelog once the subdirectory is opened (decrypted). The git log
of the outside password store will still contain the encrypted form of the full inner repository, and the inner repository does
not need to be synchronized separately. This is **not** a recommended mode of operation, but some users may prefer to do this
to retain a full human-readable history of the encrypted part of their password store.
